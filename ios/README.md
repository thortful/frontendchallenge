![Challenge Accepted](https://images-fe.thortful.com/img/hosted/challenge-accepted.jpg)

# thortful iOS Challenge

Show us what you can do!

We'd like you to write a small app with the following requirements:

* Use Swift
* Create views in code
* Make requests to Star Wars API (https://swapi.co)
* Clean and unobstructive UI, following Apple Design Guidelines
* Have a user interaction: we'd be looking at pagination and navigation to details page
* Create a readme file explaining how to build / run your app

Few notes on the app:

* Use Apple's best practices
* Write clearly and use a proper structure to write the application
* Use any 3rd party libraries you like or none at all
* Demonstrate you have tested that the app works how you want it to

Bonus points (optional):

* Create and run unit tests
* Local storage using CoreData/SwiftData/Realm/GRDB
* Version control with clean commits and branching

We could be sneaky and not say anything else, but here are some things we're looking to see:

* An app we can run ;)
* Code we can read.
* Something you'll be happy to go through with us during an interview.

### Alternatively,

If you have recently worked on a relevant project that you're able to share then we'd be happy to accept this for submission.

### Submission notes

Send us a link to a repo containing your application and build/run instructions.

---

[@thortful](https://www.thortful.com) - 2024
