![Challenge Accepted](https://images-fe.thortful.com/img/hosted/challenge-accepted.jpg)

# thortful Android Challenge

Show us what you can do!

We'd like you to write a small app with the following requirements:

* Use Kotlin and build a native Android app.
* Make requests to Star Wars API (https://swapi.co).
* Good looking UI. Use Android Material Design.
* Have a user interaction: be creative with the functionality on the page.
* Create a readme file explaining how to build / run your app.

Few notes on the app:

* Use Kotlin's and Android's best practices.
* Write clearly and use a proper structure to write the application.
* Use any 3rd party libraries you like or none at all.
* Demonstrate you have tested that the app works how you want it to.

Bonus points (optional):

* Create and run unit tests.
* Create and use a local database with Room.
* Version control with clean commits and branching.

We could be sneaky and not say anything else, but here's some things we're looking to see:

* An app we can run ;)
* Code we can read.
* Something you'll be happy to go through with us during an interview.

### Alternatively,

If you have recently worked on a relevant project that you're able to share then we'd be happy to accept this for submission.

### Submission notes

Send us a link to a repo containing your application and build/run instructions.

---

[@thortful](https://www.thortful.com) - 2024